# RedOps &middot; FOSS
> The meta group where everything comes together.

Welcome, stranger, to our team's opensource initiative.

## Projects

1.  [Quackbot](https://gitlab.com/redops-foss/rubber-duckbot/meta)

    A bot to replace your inert rubber duck by an AI powered duck, which can then help you debug your issues.
    It will have a training phase in order to understand the most common situation and ask you the right questions in order to guide you through your debug problems, just like a coworker would.

## Resources

1.  [Gitlab CI shared fragments](https://gitlab.com/redops-foss/resources/gitlab/ci/shared-fragments)

    A repository where we store our CI fragments accross our repositories, in order to avoid copy/pasting our pipelines.